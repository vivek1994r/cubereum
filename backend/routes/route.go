package routes

import (
	"github.com/zenazn/goji"
	"github.com/cubereum/common-pkg/system"
	"github.com/cubereum/backend/controller/api"
)

func PrepareRoutes(application *system.Application) {
	// Route Configurations for company
	goji.Post("/business/services/cuberium/api", application.Route(&api.Controller{}, "InsertingDataForCallBack", true, nil))
}