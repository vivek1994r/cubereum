package api

import(
	"github.com/cubereum/common-pkg/system"
	"github.com/cubereum/backend/services"
	"github.com/cubereum/backend/models"
	"github.com/zenazn/goji/web"
	"net/http"
	"encoding/json"
	"fmt"
)
type Controller struct {
	system.Controller
}

func (controller *Controller) InsertingDataForCallBack(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error) {
	decoder := json.NewDecoder(r.Body)

	var inputForReq models.InputForRequest
	err := decoder.Decode(&inputForReq)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	response, err := services.InsertingDataForCallBack(inputForReq)

	if (err != nil) {
		fmt.Println(err)
		return nil, err
	}
	return response, err
}