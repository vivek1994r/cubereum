package main

import (
	"github.com/zenazn/goji"
	"github.com/cubereum/common-pkg/system"
	"github.com/cubereum/backend/routes"
)

func main() {
	var application = &system.Application{}
	//Prepare routes
	routes.PrepareRoutes(application)
	system.ReceiveDataForInCompleteResponse()
	goji.Serve()

}

