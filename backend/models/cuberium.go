package models


type InputForRequest struct {
	Method string   `json:"method" bson:"method"`
	Url string       `json:"url" bson:"url"`
	Body  string     `json:"body" bson:"body"`
	Headers Headers  `json:"headers" bson:"headers"`
}

type Headers struct {
	ContentType string   `json:"content-type" bson:"content-type"`
	Token string       `json:"token" bson:"token"`
}

type MainModel struct {
	Id      string     `json:"_id" bson:"_id"`
	Key     string   `json:"key" bson:"key"`
	Bin     Bin       `json:"bin" bson:"bin"`
}
type Bin struct {
	WorkType string   `json:"workType" bson:"workType"`
	CreatedAt int64       `json:"createdAt" bson:"createdAt"`
	NextTrialTime int64  `json:"nextTrialTime" bson:"nextTrialTime"`
	ProcessedState bool  `json:"processedState" bson:"processedState"`
	FailureCount int  `json:"failureCount" bson:"failureCount"`
	Details Details  `json:"details" bson:"details"`

}
type Details struct {
	Method string   `json:"method" bson:"method"`
	Url string       `json:"url" bson:"url"`
	Headers Headers  `json:"headers" bson:"headers"`
	Body    string     `json:"body" bson:"body"`
	SuccessCriteria SuccessCriteria `json:"successCriteria" bson:"successCriteria"`
}
type SuccessCriteria struct {
	StatusCode int   `json:"statusCode" bson:"statusCode"`
	StatusResponse StatusResponse       `json:"statusResponse" bson:"statusResponse"`
}
type StatusResponse struct {
	Error string   `json:"error" bson:"error"`
	Body    string     `json:"body" bson:"body"`
}