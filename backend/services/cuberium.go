package services

import (
	"github.com/cubereum/common-pkg/system"
	"github.com/cubereum/backend/models"
	"encoding/json"
	"github.com/pborman/uuid"
	"time"
	"gopkg.in/mgo.v2/bson"
)

func InsertingDataForCallBack(input models.InputForRequest) ([]byte, error) {
	session,err:=system.MongoConnection()
	if err !=nil{
		return nil,err
	}
	collection:=session.DB("cubereum").C("list_of_req")
	defer session.Close()
	var dublicateData[]models.MainModel
	err=collection.Find(bson.M{"bin.details.url":input.Url}).All(&dublicateData)
	if err !=nil{
		return nil,err
	}
	response:=make(map[string]interface{})
	if len(dublicateData)>0{
		response["error"] ="you have all ready inserted this url"
		finalResponse, _ := json.Marshal(response)
		return finalResponse, nil
	}
	input1:=models.MainModel{
		Id:uuid.New(),
		Bin:models.Bin{
			WorkType:"httpRequest",
			CreatedAt:time.Now().Unix(),
			NextTrialTime:time.Now().Unix(),
			ProcessedState:false,
			FailureCount:0,
			Details:models.Details{
				Method:input.Method,
				Url:input.Url,
				Headers:input.Headers,
				Body:input.Body,
				SuccessCriteria:models.SuccessCriteria{
					StatusCode:200,
					StatusResponse:models.StatusResponse{
					Error:"",
						Body:"SUCCESS",
					},
				},
			},
		},
	}

	err=collection.Insert(&input1)
	if err !=nil{
		return nil,err
	}
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}