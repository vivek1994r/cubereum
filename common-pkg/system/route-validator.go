package system

import (
	"reflect"
	"github.com/zenazn/goji/web"
	"github.com/cubereum/backend/models"
	"net/http"
	"encoding/json"
	"gopkg.in/mgo.v2"
	"fmt"
	"time"
	"gopkg.in/mgo.v2/bson"
	"bytes"
)

type Controller struct {
}

type Application struct {
}

func (application *Application) Route(controller interface{}, route string, isPublic bool, roles []string) interface{} {

	fn := func(c web.C, w http.ResponseWriter, r *http.Request) {
		methodValue := reflect.ValueOf(controller).MethodByName(route)
		methodInterface := methodValue.Interface()

		method := methodInterface.(func(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error))
		result, err := method(c, w, r)

		if (err != nil) {
			response := make(map[string]interface{})
			response["message"] = err.Error()
			w.WriteHeader(http.StatusPreconditionFailed)
			errResponse, _ := json.Marshal(response)
			w.Write(errResponse)
		} else {
			w.WriteHeader(http.StatusOK)
			w.Write(result)
		}
	}
	return fn
}

func MongoConnection() (*mgo.Session, error) {
	session, err := mgo.Dial("localhost")
	if err != nil {
		return nil, err
	}
	return session, nil
}

func ReceiveDataForInCompleteResponse() ([]byte, error) {
	session,err:=MongoConnection()
	if err !=nil{
		return nil,err
	}
	collection:=session.DB("cubereum").C("list_of_req")
	go func() {
		min := 600 * time.Millisecond
		client := &http.Client{}
		for range time.NewTicker(min).C {
			//fmt.Println("vivek time ticker is working",time.Now().Unix())
			var dublicateData[]models.MainModel
			_=collection.Find(bson.M{"bin.nextTrialTime":bson.M{ "$lt": time.Now().Unix()},"bin.processedState":false}).All(&dublicateData)
			fmt.Println("vivek time ticker is working",dublicateData)
			if len(dublicateData)>0{
				for _,value:=range dublicateData{
					fmt.Println("value is ",value)
					values := map[string]string{
						"body":value.Bin.Details.Body,
					}
					jsonValue, _ := json.Marshal(values)
					req, err := http.NewRequest("POST", value.Bin.Details.Url, bytes.NewBuffer(jsonValue))
					if err != nil {
						fmt.Println(err)
					}
					req.Header.Set("Token", value.Bin.Details.Headers.Token)
					req.Header.Set("Content-Type", "application/json")

					response, err := client.Do(req)

					where:=bson.M{"_id":value.Id}
					if err != nil {
						timeValue:=0
						timeValue1:=1
						oldValue:=1
						count:=0
							for {
								timeValue=timeValue1+oldValue
								timeValue1 = oldValue;
								oldValue = timeValue
								count++
								if count==value.Bin.FailureCount+1{
									break
								}
							}

						update:=bson.M{"$set":bson.M{"bin.failureCount":value.Bin.FailureCount+1,"bin.nextTrialTime":value.Bin.NextTrialTime+int64(timeValue)}}
                                            _=collection.Update(where,update)
					}else{
						if response.StatusCode==200 {
							update := bson.M{"$set":bson.M{"bin.processedState":true}}
							_ = collection.Update(where, update)
						}
					}
				}

			}

		}
	}()

	return nil, nil

}
